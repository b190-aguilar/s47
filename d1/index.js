console.log("hello world")
/* Using DOM
	before, we are using the following codes in terms of selecting elements inside the html file	
*/
/*
document refers to the whole webpage and the getElementById, getElementbyClassName, getElementByTagName selects the element with the same id/class/tag as its argument
document.getElementById('txt-first-name');
document.getElementByClassName('txt-last-name'); If we have class instead of Id
document.getElementByTagName('input'); If we have tags enabled in our html file
*/

document.querySelector('#txt-first-name');
// querySelector replaces the three "get element" selectors and makes use of CSS format in terms of selecting the elements inside the html as its arguments. 
/*
	# - id
	. - class
	tagName - tag
*/

// Event Listeners
// events are all interactions of the user to our webpage; such examples are clicking, hovering, reloading, keypress, keyup
const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');

// to perform an action when an event triggers, first you have to listen to it

/*
	the function used is the addEventListener - allows a block of codes to listen to an event for them to be executed.

	addEventListener - takes 2 arguments: 	1 - a string that identifies the event to 												which the codes will listen
											2 - a function the the listener will execute, once the specified event is triggered
*/
txtFirstName.addEventListener('keyup', (event)=>{
	// innerHTML - this allows the element to record/duplicate the value of the selected variable (txtFirstname.value)
	// .value is needed. without it, the .innerHTML will only record what type of element the target variable is inside the HTML document instead of getting its value.
	spanFullName.innerHTML = txtFirstName.value;
});



