

// Event Listeners
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


txtFirstName.addEventListener('keyup', (event)=>{
	fullname(event);
});


txtLastName.addEventListener('keyup', (event) =>{
	fullname(event);
});

function fullname(input){
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
};